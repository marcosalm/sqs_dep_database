const config = {
  port: process.env.PORT || 3000,
  mysqlHost: process.env.MYSQL_HOST || '',
  mysqlPort: process.env.MYSQL_PORT || '3306',
  mysqlUser: process.env.MYSQL_USER || 'root',
  mysqlPassword: process.env.MYSQL_PASS || '',
  mysqlDatabase: process.env.MYSQL_DB || ''
};

module.exports = config;
