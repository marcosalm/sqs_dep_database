const mysql = require('mysql');
const config = require('./config');

const ENTITY_NOT_FOUNT_ERROR = 'ENTITY_NOT_FOUNT_ERROR';
const INTEGRITY_ERROR = 'INTEGRITY_ERROR';
const INTEGRITY_ERROR_CODE = 1452;
const DUPLICATED_ERROR_CODE = 1062;

/**
 * Connection configuration
 */
const databaseConfig = {
  host: config.mysqlHost,
  user: config.mysqlUser,
  password: config.mysqlPassword,
  database: config.mysqlDatabase,
  connectionLimit: 2,
  debug: false,
  acquireTimeout: 3000
};

/**
 *  Connection Pool
 */
const mysqlPool = mysql.createPool(databaseConfig);

/**
 * Gets a connection from connection Pool
 */
const createConnection = () => {
  return new Promise((resolve, reject) => {
    mysqlPool.getConnection((err, conn) => {
      if (err) {
        return reject(err);
      }
      return resolve(conn);
    });
  });
};

/**
 * Builds an Entity Not Found Error
 */
const buildEntityNotFoundError = (table, column, value, extraMessage = '') => {
  const message = `Unable to find entity in ${table} with column: ${column}, and value: ${value} \n${extraMessage}`;
  const error = new Error(message);
  error.type = ENTITY_NOT_FOUNT_ERROR;
  return error;
};

/**
 * Builds an Integrity Error
 */
const buildIntegrityError = (err) => {
  const message = `${err.message}\nsql: ${err.sql}`;
  const error = new Error(message);
  error.type = INTEGRITY_ERROR;
  return error;
};

/**
 * Checks if it is a IntegrityError
 */
const isIntegrityError = (err) => {
  let error = err;
  let result = false;

  if (err && (err.errno === INTEGRITY_ERROR_CODE || err.errno === DUPLICATED_ERROR_CODE)) {
    result = true;
    error = buildIntegrityError(err);
  }

  return { result, error };
};

module.exports.getConnection = createConnection;
module.exports.isIntegrityError = isIntegrityError;
module.exports.buildEntityNotFoundError = buildEntityNotFoundError;
module.exports.buildIntegrityError = buildIntegrityError;
module.exports.ENTITY_NOT_FOUNT_ERROR = ENTITY_NOT_FOUNT_ERROR;
module.exports.INTEGRITY_ERROR = INTEGRITY_ERROR;
module.exports.INTEGRITY_ERROR_CODE = INTEGRITY_ERROR_CODE;
module.exports.DUPLICATED_ERROR_CODE = DUPLICATED_ERROR_CODE;
